import React from 'react'

import { OkapiAppContainer } from 'mf-shared-lib'
import 'mf-shared-lib/dist/index.css'

const App = () => {
  return <OkapiAppContainer activeAppId="wibble" />
}

export default App
