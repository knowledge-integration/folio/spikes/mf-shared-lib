import React from "react";
import {
         Page,
         PageSection,
         PageHeader,
         PageHeaderTools,
         PageHeaderToolsItem,
         Nav,
         NavItem,
         NavList } from '@patternfly/react-core';

interface OkapiAppContainerProps {
  activeAppId: string,
  children: any
}

export const OkapiAppContainer = ({ activeAppId, children }: OkapiAppContainerProps) => {

  const HeaderTools = (
    <PageHeaderTools>
      <PageHeaderToolsItem>
        <Nav variant="horizontal">
          <NavList>
            <NavItem key="remote-sync" itemId={1} isActive={true}>
              <a className="pf-c-nav__link pf-m-current" href="/remote-sync">Remote Sync</a>
            </NavItem>
            <NavItem key="other1" itemId={2} isActive={false}>
              <a className="pf-c-nav__link" href="/other1">other1</a>
            </NavItem>
          </NavList>
        </Nav>
      </PageHeaderToolsItem>
    </PageHeaderTools>
  );

  const Header = (
    <PageHeader showNavToggle
                logo={`FOLIO::Next`}
                headerTools={HeaderTools}
                />
  );



 return (
    <Page mainContainerId="folioNext" header={Header} >
      <PageSection isFilled={true} hasOverflowScroll={true}>
        {children}
      </PageSection>
      <PageSection>
        <div style={{"textAlign": "right"}}>
          footer {activeAppId}
        </div>
      </PageSection>
    </Page>
  )
}
