# mf-shared-lib

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/mf-shared-lib.svg)](https://www.npmjs.com/package/mf-shared-lib) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save mf-shared-lib
```

## Usage

```tsx
import React, { Component } from 'react'

import MyComponent from 'mf-shared-lib'
import 'mf-shared-lib/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

